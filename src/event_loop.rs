use crate::{
    config::{TelegramConfig, VkConfig},
    state::WrappedState,
};
use futures::stream::iter_ok;
use hyper_tls::HttpsConnector;
use std::time::Duration;
use tbot::{methods::SendMessage, types::ParseMode::Markdown};
use tokio::prelude::*;
use vkontakte::{
    methods::wall::{Get, GetResult},
    types::Post,
};

/// Guess it's 4096 but a bit less is safer
const MAX_MESSAGE_LENGTH: usize = 4000;
const CROPPED_MESSAGE_NEGTH: usize = 3900;

type Client = hyper::Client<HttpsConnector<hyper::client::HttpConnector>>;

fn notify(telegram: TelegramConfig, post: Post) -> impl Future<Item = (), Error = ()> {
    let mut message = if post.text.is_empty() {
        let text = post
            .text
            .replace("*", "\\*")
            .replace("_", "\\_")
            .replace("[", "\\[")
            .replace("`", "\\`");

        format!(
            "New [post](https://vk.com/wall{}_{})!\n\n{}",
            post.owner_id, post.id, text,
        )
    } else {
        format!(
            "New post! vk.com/wall{}_{}",
            post.owner_id, post.id,
        )
    };

    if message.chars().count() > MAX_MESSAGE_LENGTH {
        message = message.chars().take(CROPPED_MESSAGE_NEGTH).collect::<String>() + "…";
    }

    SendMessage::new(telegram.bot_token, telegram.chat, &message)
        .parse_mode(Markdown)
        .into_future()
        .map(|_| ())
        .map_err(|error| {
            dbg!(error);
        })
}

fn process_posts(
    telegram: TelegramConfig,
    state: WrappedState,
    vk_chat: i32,
    mut posts: GetResult,
) -> impl Future<Item = (), Error = ()> {
    posts.items.retain(|post| post.is_pinned != Some(true));

    let last_post = state.lock().unwrap().last_post(vk_chat, posts.items[0].id);

    futures::stream::iter_ok(posts.items)
        .filter(move |post| post.id > last_post)
        .for_each(move |post| notify(telegram, post))
}

fn process_chat(
    state: WrappedState,
    client: &Client,
    vk_chat: i32,
    vk: VkConfig,
    telegram: TelegramConfig,
) -> impl Future<Item = (), Error = ()> {
    Get::new(&client, vk.token)
        .owner_id(vk_chat)
        .into_future()
        .map_err(|error| {
            dbg!(error);
        })
        .and_then(move |posts| process_posts(telegram, state, vk_chat, posts))
}

fn check_updates(
    vk: VkConfig,
    telegram: TelegramConfig,
    state: WrappedState,
    cpus: usize,
) -> impl Future<Item = (), Error = ()> {
    let connector = HttpsConnector::new(cpus).unwrap();
    let client = hyper::Client::builder().build(connector);

    iter_ok(vk.chats)
        .for_each(move |vk_chat| process_chat(state.clone(), &client, *vk_chat, vk, telegram))
}

pub fn start(state: WrappedState, vk: VkConfig, telegram: TelegramConfig) -> ! {
    let cpus = num_cpus::get();
    let poll_interval = Duration::from_secs(vk.poll_interval * 60);

    loop {
        tokio::run(check_updates(vk, telegram, state.clone(), cpus));
        std::thread::sleep(poll_interval);
    }
}
