use crate::config::StateConfig;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

pub type WrappedState = Arc<Mutex<State>>;

#[derive(Deserialize, Serialize)]
struct StateData {
    last_posts: HashMap<i32, u32>,
}

impl StateData {
    fn new() -> Self {
        Self {
            last_posts: HashMap::new(),
        }
    }
}

pub struct State {
    data: StateData,
    config: StateConfig,
}

impl State {
    pub fn last_post(&mut self, chat: i32, last_post: u32) -> u32 {
        let last_post = self
            .data
            .last_posts
            .insert(chat, last_post)
            .unwrap_or(last_post);

        let _ = ron::ser::to_string(&self.data)
            .ok()
            .and_then(|state| std::fs::write(&self.config.path, state).ok());

        last_post
    }
}

pub fn init(config: StateConfig) -> WrappedState {
    let data = std::fs::read(&config.path)
        .ok()
        .and_then(|contents| ron::de::from_bytes(&contents[..]).ok())
        .unwrap_or_else(StateData::new);

    let state = State { data, config };

    Arc::new(Mutex::new(state))
}
