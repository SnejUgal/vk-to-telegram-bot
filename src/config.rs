use serde::Deserialize;

mod state;
mod telegram;
mod vk;

pub use {state::*, telegram::*, vk::*};

pub struct Config {
    pub vk: VkConfig,
    pub telegram: TelegramConfig,
    pub state: StateConfig,
}

#[derive(Deserialize)]
struct OwnedConfig {
    pub vk: OwnedVkConfig,
    pub telegram: OwnedTelegramConfig,
    pub state: OwnedStateConfig,
}

impl OwnedConfig {
    fn into_static(self) -> Config {
        Config {
            vk: self.vk.into_static(),
            telegram: self.telegram.into_static(),
            state: self.state.into_static(),
        }
    }
}

pub fn parse(path: &'static str) -> Result<Config, Box<dyn std::error::Error>> {
    let contents = std::fs::read(path)?;
    let config: OwnedConfig = ron::de::from_bytes(&contents[..])?;

    Ok(config.into_static())
}
