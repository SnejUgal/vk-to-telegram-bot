mod config;
mod event_loop;
mod state;

fn main() {
    let config = config::parse("./config.ron").unwrap_or_else(|error| {
        eprintln!("Couldn't parse the config: {:#?}", error);
        std::process::exit(1);
    });

    let state = state::init(config.state);

    event_loop::start(state, config.vk, config.telegram);
}
