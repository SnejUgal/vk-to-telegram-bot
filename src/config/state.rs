use serde::Deserialize;

#[derive(Clone, Copy)]
pub struct StateConfig {
    pub path: &'static str,
}

#[derive(Deserialize)]
pub struct OwnedStateConfig {
    path: String,
}

impl OwnedStateConfig {
    pub fn into_static(self) -> StateConfig {
        StateConfig {
            path: Box::leak(Box::new(self.path)),
        }
    }
}
