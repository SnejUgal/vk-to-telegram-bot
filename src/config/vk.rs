use serde::Deserialize;

#[derive(Clone, Copy)]
pub struct VkConfig {
    pub token: &'static str,
    pub chats: &'static Vec<i32>,
    pub poll_interval: u64,
}

#[derive(Deserialize)]
pub struct OwnedVkConfig {
    token: String,
    chats: Vec<i32>,
    poll_interval: u64,
}

impl OwnedVkConfig {
    pub fn into_static(self) -> VkConfig {
        VkConfig {
            token: Box::leak(Box::new(self.token)),
            chats: Box::leak(Box::new(self.chats)),
            poll_interval: self.poll_interval,
        }
    }
}
